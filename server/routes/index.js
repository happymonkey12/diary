var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
var ObjectID = require('mongodb').ObjectID;
var dbConnection = require('../db.js');
const bcrypt = require('bcrypt');
require('dotenv').config();

const EXPRESS_JWT_KEY = process.env.EXPRESS_JWT_KEY;

/* GET home page. */
router.delete('/posts/:id', verifyToken, async function(req, res, next) {
  const username = res.locals.userData.username;
  
  try {
    const db = dbConnection.getConnection().db("diary");

    await db.collection("posts").deleteOne({
      username: username,
      _id: new ObjectID(req.params.id)
    });

    res.json({
      status: "OK"
    });
  } catch (err) {
    res.status(500).json({
      status: "ERROR"
    });
  }
});

router.put('/posts/:id', verifyToken, async function(req, res, next) {
  const username = res.locals.userData.username;
  
  try {
    const db = dbConnection.getConnection().db("diary");

    await db.collection("posts").updateOne({
      username: username,
      _id: new ObjectID(req.params.id)
    }, {
      $set: {
        title: req.body.title,
        content: req.body.content
      }
    });

    res.json({
      status: "OK"
    });
  } catch (err) {
    res.status(500).json({
      status: "ERROR"
    });
  }
});

router.post('/posts', verifyToken, async function(req, res, next) {
  try {
    const db = dbConnection.getConnection().db("diary");

    await db.collection("posts").insertOne({
      username: res.locals.userData.username,
      title: req.body.title,
      content: req.body.content,
      createDate: new Date()
    });

    res.json({
      status: "OK"
    });
  } catch (err) {
    res.status(500).json({
      status: "ERROR"
    });
  }
});

router.get('/posts', verifyToken, async function(req, res, next) {
  const username = res.locals.userData.username;
  
  try {
    const db = dbConnection.getConnection().db("diary");
    const posts = await db.collection("posts").find({username: username}).sort({createDate: -1}).toArray();

    res.json({
      posts: posts,
      status: "OK"
    });
  } catch (err) {
    res.status(500).json({
      status: "ERROR"
    });
  }
});

router.get('/posts/:id', verifyToken, async function(req, res, next) {
  const username = res.locals.userData.username;
  
  try {
    const db = dbConnection.getConnection().db("diary");
    const post = await db.collection("posts").findOne({
      username: username,
      _id: new ObjectID(req.params.id)
    });

    res.json({
      post: post,
      status: "OK"
    });
  } catch (err) {
    res.status(500).json({
      status: "ERROR"
    });
  }
});

function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];

  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const token = bearer[1];
    
    try {
      const userData = jwt.verify(token, EXPRESS_JWT_KEY);
      res.locals.userData = userData;
      next();
    } catch (err) {
      res.status(403).json({
        status: "INVALID_TOKEN"
      });
    }
  } else {
    res.status(403).json({
      status: "INVALID_TOKEN"
    });
  }
}

router.post('/login', async function(req, res, next) {
  try {
    let loginSuccess = false;

    const db = dbConnection.getConnection().db("diary");
    const user = await db.collection("users").findOne({
      username: req.body.username
    });

    if (user !== null) {
      const match = await bcrypt.compare(req.body.password, user.password);

      if (match) {
        var token = jwt.sign({username: user.username}, EXPRESS_JWT_KEY);
        
        loginSuccess = true;

        res.json({
          token: token,
          username: user.username,
          status: "OK"
        });
      }
    } 
    
    if (!loginSuccess) {
      res.json({
        status: "FAIL"
      });
    }
  } catch (err) {
    res.status(500).json({
      status: "ERROR"
    });
  }
});

router.post('/register', async function(req, res, next) {
  try {
    const db = dbConnection.getConnection().db("diary");
    const userExists = await db.collection("users").countDocuments({username: req.body.username});

    if (userExists !== 0) {
      res.json({
        status: "EXISTS"
      });
    } else {
      const hash = await bcrypt.hash(req.body.password, 10);
      const insertRes = await db.collection("users").insertOne({
        username: req.body.username,
        password: hash
      });

      if (insertRes.insertedCount !== 1) {
        throw "Insert Error";
      }

      res.json({
        status: "OK"
      });
    }
  } catch (err) {
    res.status(500).json({
      status: "ERROR"
    });
  }
});

module.exports = router;
