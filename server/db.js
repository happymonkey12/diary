var MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
var connection;

const dbConnection = {
	connect: function() {
		return new Promise(function(resolve, reject) {
			MongoClient.connect(url, function(err, db) {
				if (err) {
					reject(err);
				} else {
					connection = db;
					resolve();
				}
			});
		});
	},
	getConnection: function () {
		return connection;
	}
};

module.exports = dbConnection;