var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var app = express();
var cors = require('cors');

require('dotenv').config();

app.use(cors({
  origin: process.env.ALLOWED_ORIGIN
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/diary', indexRouter);

app.use(function(req, res, next) {
  res.sendStatus(404);
});

module.exports = app;
