import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { isAuthenticated } from './Authentication.js';
import Login from './Login.js';
import Register from './Register.js';
import Dashboard from './Dashboard.js';
import NewPost from './NewPost.js';
import EditPost from './EditPost.js';

function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route
					path="/login"
					render={(props) => isAuthenticated() ? <Redirect to="/" /> : <Login {...props} />}
				/>
				<Route
					path="/register"
					render={(props) => isAuthenticated() ? <Redirect to="/" /> : <Register {...props} />}
				/>
				<Route
					exact path="/"
					render={(props) => isAuthenticated() ? <Dashboard {...props} /> : <Redirect to="login" />}
				/>
				<Route
					exact path="/new"
					render={(props) => isAuthenticated() ? <NewPost {...props} /> : <Redirect to="login" />}
				/>
				<Route
					exact path="/edit"
					render={(props) => isAuthenticated() ? <EditPost {...props} /> : <Redirect to="login" />}
				/>
			</Switch>
		</BrowserRouter>
	);
}

export default App;