import React from 'react';
import './DiaryEntry.css';

class DiaryEntry extends React.Component {
	constructor(props) {
		super(props);
		this.handleEditPost = this.handleEditPost.bind(this);
	}

	handleEditPost(id) {
		this.props.history.push({
			pathname: '/edit',
			id: id
		});
	}

	render() {
		return (
			<div id="frame" onClick={() => this.handleEditPost(this.props.id)}>	
				<div id="title"><b>{this.props.title}</b></div>
				<div id="createDateTime">{this.props.createDateTime}</div>
				<div id="content">{this.props.content}</div>
				<div style={{clear: "both"}}></div>
			</div >
		);
	}
}

export default DiaryEntry;