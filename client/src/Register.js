import React from 'react';
import { Link } from "react-router-dom";
import "./Login.css";
import { BASE_URL } from './Endpoints.js';

class Register extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			username: "",
			password: "",
			message: ""
		};

		this.handleUsernameChange = this.handleUsernameChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
		this.handleRegister = this.handleRegister.bind(this);
	}

	handleUsernameChange(event) {
		this.setState({ username: event.target.value });
	}

	handlePasswordChange(event) {
		this.setState({ password: event.target.value });
	}

	async handleRegister() {
		if (this.state.username === "" || this.state.password === "") {
			this.setState({ message: "Username and password cannot be empty." });
		} else if (this.state.username.length < 6 || this.state.password.length < 6) {
			// TODO move this check to server side
			this.setState({ message: "Username and password must be at least 6 characters in length." });
		} else {
			try {
				const response = await fetch(BASE_URL + '/register', {
					method: "POST",
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						username: this.state.username,
						password: this.state.password
					})
				});

				let data = await response.json();

				switch (data.status) {
					case "OK":
						this.setState({ message: "Account successfuly created. Please return to Login page and login." });
						break;

					case "EXISTS":
						this.setState({ message: "Username has already been taken" });
						break;

					default:
						this.setState({ message: "Something went wrong, please try again" });
						break;
				}
			} catch (err) {
				this.setState({ message: "Registeration failed" });
			}
		}
	}

	render() {
		return (
			<div className="App">
				<h3>Diary</h3>
				<label className="label">Username</label>
				<input className="inputLogin" type="text" name="username" value={this.state.username} onChange={this.handleUsernameChange} required />
				<br />
				<label className="label">Password</label>
				<input className="inputLogin" type="password" name="password" style={{marginLeft: "4px", marginTop: "5px"}} value={this.state.password} onChange={this.handlePasswordChange} required />
				<br />
				{this.state.message !== "" ? <p>{this.state.message}</p> : ""}
				<div id="buttonDiv">
					<button id="loginButton" onClick={this.handleRegister}>Register</button>
				</div>
				<p>Have an account? <Link to="/login">Return to Login</Link></p>
			</div>
		);
	}
}

export default Register;