import React from 'react';
import { Link } from "react-router-dom";
import { getToken } from './Authentication.js';
import { BASE_URL } from './Endpoints.js';
import "./EditPost.css";

class NewPost extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			title: "",
			content: ""
		}

		this.handleTitleChange = this.handleTitleChange.bind(this);
		this.handleContentChange = this.handleContentChange.bind(this);
		this.handleSubmitPost = this.handleSubmitPost.bind(this);
	}

	handleTitleChange(event) {
		this.setState({ title: event.target.value });
	}

	handleContentChange(event) {
		this.setState({ content: event.target.value });
	}

	async handleSubmitPost() {
		try {
			await fetch(BASE_URL + '/posts', {
				method: "POST",
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + getToken()
				},
				body: JSON.stringify({
					title: this.state.title,
					content: this.state.content
				})
			});
		} catch (err) {
			console.log(err);
		}

		this.props.history.replace('/');
	}

	render() {
		return (
			<div className="App">
				<h2>New Post</h2>
				<input className="editTitle" name="title" type="text"  placeholder="title" value={this.state.title} onChange={this.handleTitleChange} />
				<br />
				<textarea className="editContent" name="content" placeholder="content" value={this.state.content} onChange={this.handleContentChange} />
				<br /><br />
				<button className="editButton" onClick={this.handleSubmitPost}>Submit Post</button>
				<Link to="/"><button className="backButton">Back</button></Link>
			</div>
		);
	}
}

export default NewPost;