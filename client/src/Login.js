import React from 'react';
import { Link } from "react-router-dom";
import "./Login.css";
import { BASE_URL } from './Endpoints.js';

class Login extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			username: "",
			password: "",
			message: ""
		};

		this.handleUsernameChange = this.handleUsernameChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
	}

	handleUsernameChange(event) {
		this.setState({ username: event.target.value });
	}

	handlePasswordChange(event) {
		this.setState({ password: event.target.value });
	}

	async handleLogin() {
		try {
			const response = await fetch(BASE_URL + '/login', {
				method: "POST",
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					username: this.state.username,
					password: this.state.password
				})
			});

			let data = await response.json();

			switch (data.status) {
				case "OK":
					localStorage.setItem("token", data.token);
					localStorage.setItem("currentUser", data.username);
					this.props.history.replace('/');
					break;

				case "FAIL":
					this.setState({ message: "Incorrect username/password" });
					break;

				default:
					this.setState({ message: "Login failed, please try again." });
					break;
			}
		} catch (err) {
			this.setState({ message: "Login failed, please try again." });
		}
	}

	render() {
		return (
			<div className="App">
				<h3>Diary</h3>
				<label className="label">Username</label>
				<input className="inputLogin" type="text" name="username" value={this.state.username} onChange={this.handleUsernameChange} required />
				<br />
				<label className="label">Password</label>
				<input className="inputLogin" type="password" name="password" style={{marginLeft: "4px", marginTop: "5px"}} value={this.state.password} onChange={this.handlePasswordChange} required />
				<br />
				{this.state.message !== "" ? <p>{this.state.message}</p> : ""}
				<div id="buttonDiv">
					<button id="loginButton" onClick={this.handleLogin}>Login</button>
				</div>
				<p>Don't have an account? <Link to="/register"> Create an Account</Link></p>
			</div>
		);
	}
}

export default Login;