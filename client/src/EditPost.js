import React from 'react';
import { Link } from "react-router-dom";
import { getToken } from './Authentication.js';
import { BASE_URL } from './Endpoints.js';
import "./EditPost.css";

class EditPost extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			title: "",
			content: ""
		}

		if (this.props.location.id === undefined) {
			this.props.history.replace('/');
		} else {
			this.handleTitleChange = this.handleTitleChange.bind(this);
			this.handleContentChange = this.handleContentChange.bind(this);
			this.handleEditPost = this.handleEditPost.bind(this);
			this.handleDeletePost = this.handleDeletePost.bind(this);
		}
	}

	async componentDidMount() {
		try {
			const response = await fetch(BASE_URL + '/posts/' + this.props.location.id, {
				method: "GET",
				headers: { 'Authorization': 'Bearer ' + getToken() }
			});

			const data = await response.json();

			if (data.status === "OK") {
				this.setState({
					title: data.post.title,
					content: data.post.content
				})
			}
		} catch (err) {
			console.log(err);
		}
	}

	handleTitleChange(event) {
		this.setState({ title: event.target.value });
	}

	handleContentChange(event) {
		this.setState({ content: event.target.value });
	}

	async handleEditPost() {
		try {
			await fetch(BASE_URL + '/posts/' + this.props.location.id, {
				method: "PUT",
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + getToken()
				},
				body: JSON.stringify({
					title: this.state.title,
					content: this.state.content
				})
			});
		} catch (err) {
			console.log(err);
		}

		this.props.history.replace('/');
	}

	async handleDeletePost() {
		try {
			await fetch(BASE_URL + '/posts/' + this.props.location.id, {
				method: "DELETE",
				headers: {
					'Authorization': 'Bearer ' + getToken()
				}
			});
		} catch (err) {
			console.log(err);
		}

		this.props.history.replace('/');
	}

		render() {
			return (
				<div className="App">
					<h2>Edit Post</h2>
					<input className="editTitle" name="title" type="text" value={this.state.title} onChange={this.handleTitleChange} />
					<br />
					<textarea className="editContent" name="content" value={this.state.content} onChange={this.handleContentChange} />
					<br /><br />
					<button className="editButton" onClick={this.handleEditPost}>Update Post</button>
					<button className="editButton" onClick={this.handleDeletePost}>Delete Post</button>
					<Link to="/"><button className="backButton">Back</button></Link>
				</div>
			);
		}
	}

	export default EditPost;