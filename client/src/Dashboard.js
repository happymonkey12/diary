import React from 'react';
import { getToken, logout, getCurrentUser } from './Authentication.js';
import DiaryEntry from './DiaryEntry.js';
import './Dashboard.css';
import { BASE_URL } from './Endpoints.js';

class Dashboard extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			diaryEntries: []
		}

		this.handleLogout = this.handleLogout.bind(this);
		this.handleNewPost = this.handleNewPost.bind(this);
	}

	async componentDidMount() {
		try {
			const response = await fetch(BASE_URL + '/posts', {
				method: "GET",
				headers: { 'Authorization': 'Bearer ' + getToken() }
			});

			const data = await response.json();

			if (data.status === "OK") {
				this.setState({ diaryEntries: data.posts })
			}
		} catch (err) {
			console.log(err);
		}
	}

	handleLogout() {
		logout();
		this.props.history.replace('/login');
	}

	handleNewPost() {
		this.props.history.push('/new');
	}

	render() {
		const diaryEntries = this.state.diaryEntries.map(data => {
			const dateString = data.createDate.split('T')[0].replace(/-/g, '/');
			const timeString = data.createDate.split('T')[1].split('.')[0];

			return (
				<DiaryEntry title={data.title} content={data.content} createDateTime={dateString + " @ " + timeString} id={data._id} history={this.props.history} />
			);
		});

		return (
			<div className="App">
				<h2 id="header">Hi {getCurrentUser()}</h2>
				<button id="newPost" onClick={this.handleNewPost}>New Post</button>
				<button id="logout" onClick={this.handleLogout}>Logout</button>
				<p id="about">Note: Edit an existing post by clicking on it.<br/>To view this project's source code, <a href="https://bitbucket.org/happymonkey12/diary/src/master/" target="_blank" rel="noopener noreferrer">click here.</a></p>
				<div style={{clear: "both"}}> 
					{diaryEntries}
				</div>
			</div>
		);
	}
}

export default Dashboard;