export function isAuthenticated() {
    return getToken() !== null && getCurrentUser() !== null;
}

export function getToken() {
    return localStorage.getItem("token");
}

export function getCurrentUser() {
    return localStorage.getItem("currentUser");
}

export function logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("currentUser");
}